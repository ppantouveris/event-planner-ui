import {toLocalISOString} from "@/utils/dateTimeHelpers";

function storeRefreshToken(response) {
    const refreshToken = response.headers.get('Refresh-Token');
    if (refreshToken) {
        // Update the stored token with the new one
        localStorage.setItem('jwtToken', refreshToken);
    }
}

function addHeaders(token) {
    return {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
    };
}

export const fetchAllEvents = async () => {
    try {
        const token = localStorage.getItem('authToken');
        // Corrected: Added await to wait for the fetch to complete
        const response = await fetch('http://localhost:8080/api/events', {
            method: 'GET',
            headers: addHeaders(token)
        });

        // The response must be checked for its ok status before proceeding
        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        // To correctly use storeRefreshToken, ensure the response headers are accessible
        // and CORS policies allow access to the Refresh-Token header.
        storeRefreshToken(response);

        // Corrected: Await the parsing of the JSON body
        return await response.json();
    } catch (error) {
        console.error('Error fetching data:', error);
        throw error; // It's a good practice to re-throw the error for further handling.
    }
};


export const fetchEventByUuid = async (uuid) => {
    try {
        const token = localStorage.getItem('authToken');
        const response = await fetch(`http://localhost:8080/api/events/${uuid}`, {
            method: 'GET',
            headers: addHeaders(token)
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        storeRefreshToken(response);
        return await response.json();
    } catch (error) {
        console.error('Error fetching event data:', error);
    }
};

export const editEventByUuid = async (event) => {
    const token = localStorage.getItem('authToken');
    event.time = toLocalISOString(event.time);

    const response = await fetch('http://localhost:8080/api/events', {
        method: 'PUT',
        headers: addHeaders(token),
        body: JSON.stringify(event),
    });

    if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }

    storeRefreshToken(response);
    return await response.json();
}

export const createEvent = async (event) => {
    const token = localStorage.getItem('authToken');
    event.time = toLocalISOString(event.time);

    const response = await fetch('http://localhost:8080/api/events', {
        method: 'POST',
        headers: addHeaders(token),
        body: JSON.stringify(event),
    });

    if (!response.ok) {
        throw new Error(`HTTP error! Status: ${response.status}`);
    }

    storeRefreshToken(response);
    return await response.json();
}

export const deleteEvent = async (uuid) => {
    try {
        const token = localStorage.getItem('authToken');
        const response = await fetch(`http://localhost:8080/api/events/${uuid}`, {
            method: 'DELETE',
            headers: addHeaders(token)
        });

        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        storeRefreshToken(response);
    } catch (error) {
        console.error('Error fetching event data:', error);
    }
}

export const signInUser = async (email, password) => {
    const response = await fetch('http://localhost:8080/api/auth/signin', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
    });

    const data = await response.json();

    if (!response.ok) {
        throw new Error(data.message || 'Login failed. Please try again');
    }

    // Assuming the API returns a token upon successful authentication
    // Store the token in localStorage or a cookie
    localStorage.setItem('authToken', data.token);

    return data;
}

export const isTokenExpired = async (token) => {
    try {
        const response = await fetch('http://localhost:8080/api/auth/token/validate', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ token }),
        });

        const data = await response.json();

        if (!response.ok) {
            throw new Error(data.message || 'An error occurred!');
        }

        return data.isExpired; // You might want to return something (e.g., user data, token)
    } catch (error) {
        console.error('There was a problem with your fetch operation:', error);
        throw error; // Rethrow the error so you can catch it in the calling component
    }
}


// Function to decode a base64-url string
function base64UrlDecode(str) {
    // Replace non-url compatible chars with base64 standard chars
    let output = str.replace(/-/g, '+').replace(/_/g, '/');

    // Pad out with standard base64 required padding characters
    const pad = output.length % 4;
    if (pad) {
        if (pad === 1) {
            throw new Error('InvalidLengthError: Input base64url string is the wrong length to determine padding');
        }
        output += new Array(5-pad).join('=');
    }

    return window.atob(output);
}

// Function to parse JWT and extract payload
export function parseJwt() {
    const token = localStorage.getItem('authToken');
    if (token) {
        const base64Url = token.split('.')[1]; // Get payload part
        const base64 = base64UrlDecode(base64Url);
        return JSON.parse(base64);
    }

    return null
}