import React, {createContext, useContext, useEffect, useState} from 'react';
import {useRouter} from "next/router";
import {isTokenExpired, signInUser} from "@/services/service";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
    const [isAuthenticated, setIsAuthenticated] = useState(false);
    const [loading, setLoading] = useState(true);
    const router = useRouter();

    useEffect(() => {
        // Check for token in storage and validate it to set user state
        const validateToken = async () => {
            try {
                const token = localStorage.getItem('authToken');
                if (token) {
                    if (!await isTokenExpired(token)) {
                        setIsAuthenticated(true);
                        return
                    }

                    // get is token expired. If expired empty storage and redirect to sing in and setIsAuthenticated
                    localStorage.removeItem('authToken');
                    setIsAuthenticated(false);
                    router.push('/login');
                }
            } catch (error) {
                localStorage.removeItem('authToken');
                setIsAuthenticated(false);
                console.error(error);
            } finally {
                setLoading(false);
            }
        };

        validateToken();
    }, []);

    const signIn = async (email, password) => {
        try {
            const data = await signInUser(email, password);
            setIsAuthenticated(true);

            return data; // You might want to return something (e.g., user data, token)
        } catch (error) {
            console.error('There was a problem with your fetch operation:', error);
            throw error; // Rethrow the error so you can catch it in the calling component
        }
    };

    const signOut = () => {
        // Remove the token from storage and reset user state
        localStorage.removeItem('authToken');
        setIsAuthenticated(false);
    };

    return (
        <AuthContext.Provider value={{ isAuthenticated, loading, signIn, signOut }}>
            {children}
        </AuthContext.Provider>
    );
};

// Custom hook to use the auth context
export const useAuth = () => useContext(AuthContext);
