export const toLocalISOString = (date) => {
    if (!(date instanceof Date)) {
        date = new Date(date);
    }

    // Format the date part (YYYY-MM-DDTHH:mm:ss)
    return date.getFullYear() +
        "-" + String(date.getMonth() + 1).padStart(2, '0') +
        "-" + String(date.getDate()).padStart(2, '0') +
        "T" + String(date.getHours()).padStart(2, '0') +
        ":" + String(date.getMinutes()).padStart(2, '0') +
        ":" + String(date.getSeconds()).padStart(2, '0');
}
