import React from 'react';

const Notification = ({ message, type }) => {
    if (!message) return null;

    const color = type === 'error' ? 'red' : 'green';

    return (
        <div style={{ color: color, margin: '10px 0', padding: '10px', border: `1px solid ${color}`, borderRadius: '5px' }}>
            {message}
        </div>
    );
};

export default Notification;