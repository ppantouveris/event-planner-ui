import {useEffect, useState} from "react";
import Event from "@/components/ui/Event";
import {fetchAllEvents} from "@/services/service";

const EventList = () => {
    const [events, setEvents] = useState(null);

    // Define a function to fetch events
    const fetchEvents = async () => {
        const fetchedEvents = await fetchAllEvents();
        setEvents(fetchedEvents);
    };

    useEffect(() => {
        fetchEvents();
    }, [])

    if (!events) {
        return <div>Loading...</div>
    }

    return <div>
        {events.map((event, index) => (
            <Event key={index} event={event} fetchEvents={fetchEvents} />
        ))}
    </div>
}

export default EventList;