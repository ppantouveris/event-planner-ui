import React, {useState} from 'react';
import {ErrorMessage, Field, Form, Formik} from 'formik';
import * as Yup from 'yup';
import Notification from './Notification';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import styles from "./CreateEventForm.module.css"
import {createEvent} from "@/services/service";


// Validation schema
const CreateEventSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    location: Yup.string()
        .min(2, 'Too Short!')
        .max(100, 'Too Long!')
        .required('Required'),
    time: Yup.date().required('Required'),
    maxParticipants: Yup.number()
        .required('Required'),
    availableSlots: Yup.number()
        .required('Required'),
});

const CreateEvent = () => {
    const [notification, setNotification] = useState({message: '', type: ''});

    const handleSubmit = async (values, {setSubmitting}) => {
        try {
            await createEvent(values);
            setNotification({message: 'Event created successfully!', type: 'success'});
        } catch (error) {
            console.error('Error during signup:', error);
            setNotification({message: 'Event creation failed. Please try again.', type: 'error'});
        } finally {
            setSubmitting(false);
        }
    };

    return (
        <div className={styles.mainContainer}>
            <h2 className={styles.title}>Create Event</h2>
            <Notification message={notification.message} type={notification.type}/>
            <Formik initialValues={{name: '', location: '', time: new Date(), maxParticipants: 0, availableSlots: 0}}
                    validationSchema={CreateEventSchema} onSubmit={handleSubmit}>
                {({isSubmitting, setFieldValue, values}) => (
                    <Form className={styles.form}>
                        <div className={styles.fieldContainer}>
                            <label htmlFor="name">Event Name</label>
                            <div className={styles.fieldContent}>
                                <Field type="text" name="name"/>
                                <ErrorMessage name="name" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <div className={styles.fieldContainer}>
                            <label htmlFor="location">Event Location</label>
                            <div className={styles.fieldContent}>
                                <Field type="text" name="location"/>
                                <ErrorMessage name="location" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <div className={styles.fieldContainer}>
                            <label htmlFor="time">Time</label>
                            <div className={styles.fieldContent}>
                                <DatePicker
                                    selected={values.time}
                                    onChange={(date) => setFieldValue('time', date)}
                                    showTimeSelect
                                    dateFormat="Pp" // Adjust date format as needed
                                />
                                <ErrorMessage name="time" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <div className={styles.fieldContainer}>
                            <label htmlFor="maxParticipants">Maximum number of participants</label>
                            <div className={styles.fieldContent}>
                                <Field type="number" name="maxParticipants"/>
                                <ErrorMessage name="maxParticipants" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <div className={styles.fieldContainer}>
                            <label htmlFor="availableSlots">Available slots</label>
                            <div className={styles.fieldContent}>
                                <Field type="number" name="availableSlots"/>
                                <ErrorMessage name="availableSlots" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <button className={styles.button} type="submit" disabled={isSubmitting}>
                            Submit
                        </button>
                    </Form>
                )}
            </Formik>
        </div>
    );
};

export default CreateEvent;