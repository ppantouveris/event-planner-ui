import styles from './Event.module.css';
import {useRouter} from "next/router";
import {useState} from 'react';
import ConfirmationModal from './ConfirmationModal';
import {deleteEvent as deleteEventService} from "@/services/service";
import {parseJwt} from "@/services/service";


const Event = ({key, event, fetchEvents}) => {
    const [isModalOpen, setIsModalOpen] = useState(false);
    const router = useRouter();
    const userRoles = parseJwt()?.roles;
    const editEvent = () => {
        router.push(`/edit/${event.uuid}`);
    }

    const deleteEvent = async () => {
        console.log("Deleting event:", event.uuid);
        await deleteEventService(event.uuid);
        fetchEvents();
        setIsModalOpen(false); // Close modal after action
    };

    return <div id={key} className={styles.eventContainer}>
        <div className={styles.eventTitle}>{event.name}</div>
        <div>{event.location} @ {event.time}</div>
        <div>Total number of participants: {event.maxParticipants}</div>
        <div>Available slots: {event.availableSlots}</div>
        {
            userRoles.includes("ROLE_ADMIN") &&
            (
                <div className={styles.buttonsContainer}>
                    <button className={`${styles.button} ${styles.deleteButton}`} onClick={() => setIsModalOpen(true)}>Delete</button>
                    <button className={`${styles.button} ${styles.editButton}`} onClick={editEvent}>Edit</button>
                    <ConfirmationModal isOpen={isModalOpen} message="Are you sure you want to delete this event?"
                                   onConfirm={deleteEvent} onCancel={() => setIsModalOpen(false)}/>
                </div>
            )
        }

    </div>
}

export default Event;