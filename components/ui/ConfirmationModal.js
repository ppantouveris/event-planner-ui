import styles from './ConfirmationModal.module.css';

const ConfirmationModal = ({ isOpen, message, onConfirm, onCancel }) => {
    if (!isOpen) return null;

    return (
        <div className={styles.modalOverlay}>
            <div className={styles.modal}>
                <p className={styles.message}>{message}</p>
                <button className={styles.confirmButton} onClick={onConfirm}>Confirm</button>
                <button className={styles.cancelButton} onClick={onCancel}>Cancel</button>
            </div>
        </div>
    );
};

export default ConfirmationModal;