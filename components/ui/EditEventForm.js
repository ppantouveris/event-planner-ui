import React, {useEffect, useState} from 'react';
import {ErrorMessage, Field, Form, Formik, useFormikContext} from 'formik';
import * as Yup from 'yup';
import Notification from './Notification';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import styles from "./CreateEventForm.module.css"
import {editEventByUuid} from "@/services/service";

// Validation schema
const EditEventSchema = Yup.object().shape({
    name: Yup.string()
        .min(2, 'Too Short!')
        .max(50, 'Too Long!')
        .required('Required'),
    location: Yup.string()
        .min(2, 'Too Short!')
        .max(100, 'Too Long!')
        .required('Required'),
    time: Yup.date().required('Required'),
    maxParticipants: Yup.number()
        .required('Required'),
    availableSlots: Yup.number()
        .required('Required'),
});

// Custom DatePicker component to integrate with Formik
const FormikDatePicker = ({ name }) => {
    const { setFieldValue, values } = useFormikContext();
    return (
        <DatePicker
            selected={(values[name] && new Date(values[name])) || null}
            onChange={date => setFieldValue(name, date)}
            showTimeSelect
            dateFormat="Pp" // Adjust date format as needed
        />
    );
};

const EditEvent = ({ eventDetails }) => {
    const [notification, setNotification] = useState({ message: '', type: '' });

    // Ensure eventDetails.time is converted to Date object for DatePicker
    useEffect(() => {
        if (eventDetails.time && !(eventDetails.time instanceof Date)) {
            eventDetails.time = new Date(eventDetails.time);
        }
    }, [eventDetails]);

    const handleSubmit = async (values, { setSubmitting }) => {
        try {
            await editEventByUuid(values)
            setNotification({ message: 'Event updated successfully!', type: 'success' });
        } catch (error) {
            console.error('Error during signup:', error);
            setNotification({ message: 'Event update failed. Please try again.', type: 'error' });
        } finally {
            setSubmitting(false);
        }
    };

    return (
        <div className={styles.mainContainer}>
            <h2 className={styles.title}>Edit Event</h2>
            <Notification message={notification.message} type={notification.type} />
            <Formik initialValues={eventDetails}  validationSchema={EditEventSchema} onSubmit={handleSubmit}>
                {({ isSubmitting }) => (
                    <Form className={styles.form}>
                        <div className={styles.fieldContainer}>
                            <label htmlFor="name">Event Name</label>
                            <div className={styles.fieldContent}>
                                <Field type="text" name="name"/>
                                <ErrorMessage name="name" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <div className={styles.fieldContainer}>
                            <label htmlFor="location">Event Location</label>
                            <div className={styles.fieldContent}>
                                <Field type="text" name="location"/>
                                <ErrorMessage name="location" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <div className={styles.fieldContainer}>
                            <label htmlFor="time">Time</label>
                            <div className={styles.fieldContent}>
                                <FormikDatePicker name="time"/>
                                <ErrorMessage name="time" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <div className={styles.fieldContainer}>
                            <label htmlFor="maxParticipants">Maximum number of participants</label>
                            <div className={styles.fieldContent}>
                                <Field type="number" name="maxParticipants"/>
                                <ErrorMessage name="maxParticipants" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <div className={styles.fieldContainer}>
                            <label htmlFor="availableSlots">Available slots</label>
                            <div className={styles.fieldContent}>
                                <Field type="number" name="availableSlots"/>
                                <ErrorMessage name="availableSlots" component="div" className={styles.errorMessage} />
                            </div>
                        </div>

                        <button className={styles.button} type="submit" disabled={isSubmitting}>
                            Submit
                        </button>
                    </Form>
                )}
            </Formik>
        </div>
    );
};

export default EditEvent;