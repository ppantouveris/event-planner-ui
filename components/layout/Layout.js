import Header from "@/components/layout/Header";
import Footer from "@/components/layout/Footer";
import styles from './Layout.module.css';
import {useAuth} from "@/context/AuthContext";
import LoginPage from "@/pages/login";

export default function Layout({children}) {
    const { isAuthenticated } = useAuth();

    // If not authenticated, render only the SignIn component
    if (!isAuthenticated) {
        return <div className={styles.mainContent}>
            <LoginPage />
        </div>;
    }

    return <div className={styles.mainContent}>
        <Header/>
        <div className={styles.page}>
            {children}
        </div>
        <Footer/>
    </div>
}

