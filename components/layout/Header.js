import Link from "next/link";
import styles from './Header.module.css';
import {useAuth} from "@/context/AuthContext";
import {useRouter} from "next/router";
import {useState} from "react";
import {parseJwt} from "@/services/service";

export default function Header() {
    const {signOut} = useAuth();
    const router = useRouter();
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const userRoles = parseJwt()?.roles;

    const handleSignOut = async () => {
        // Perform sign out actions here, e.g., clear auth token, redirect, etc.
        await signOut();
        // Redirect or update UI upon successful authentication
        router.push('/'); // Adjust the redirect path as needed
    };

    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    }

    return (
        <>
            <header className={styles.header}>
                <div className={styles.headerTitle}>Events app</div>
                <button onClick={toggleMenu} className={styles.menuToggle}>
                    <div className={styles.bar}></div>
                    <div className={styles.bar}></div>
                    <div className={styles.bar}></div>
                </button>
            </header>
            {isMenuOpen && (
                <div className={styles.fullScreenMenu}>
                    <button onClick={toggleMenu} className={styles.closeMenu}>X</button>
                    <Link onClick={toggleMenu} href="/" className={styles.link}>Home</Link>
                    {
                        userRoles.includes("ROLE_ADMIN") && (
                            <Link onClick={toggleMenu} href="/create" className={styles.link}>Create Event</Link>
                        )
                    }
                    <button onClick={handleSignOut} className={styles.signOutButton}>Sign Out</button>
                </div>
            )}
        </>
    )
}