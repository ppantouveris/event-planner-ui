import {useEffect} from 'react';
import {useRouter} from 'next/router';
import {useAuth} from "@/context/AuthContext";
import EventList from "@/components/ui/EventList";

export default function HomePage() {
    const { isAuthenticated, loading } = useAuth();
    const router = useRouter();

    useEffect(() => {
        if (!loading && !isAuthenticated) router.push('/login'); // Redirect to login if not authenticated
    }, [isAuthenticated, loading, router]);

    if (loading) return <div>Loading...</div>;

    return <div>
        <EventList />
    </div>
}