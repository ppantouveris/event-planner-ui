import {useState} from 'react';
import {useRouter} from 'next/router'; // For redirecting after successful login
import {useAuth} from "@/context/AuthContext";
import styles from "./login.module.css";

const LoginPage = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const router = useRouter();

    // Use useAuth hook to get signIn method
    const {signIn} = useAuth();

    const handleSubmit = async (e) => {
        e.preventDefault();
        setIsLoading(true);
        setError(''); // Reset error state

        try {
            // Use signIn method from auth context
            await signIn(email, password);
            setIsLoading(false);
            // Redirect or update UI upon successful authentication
            router.push('/'); // Adjust the redirect path as needed
        } catch (error) {
            setError(error.message || 'Failed to login');
            setIsLoading(false);
        }
    }

    return (
        <div className={styles.formContainer}>
            <h1>Event Planner</h1>
            <form onSubmit={handleSubmit} className={styles.loginForm}>
                <input type="email" className={styles.input} placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)}/>
                <input type="password" className={styles.input} placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)}/>
                <button type="submit" disabled={isLoading} className={styles.button}>{isLoading ? 'Signing In...' : 'Sign In'}</button>
                {error && <p style={{color: 'red'}}>{error}</p>}
            </form>
        </div>

    );
};

export default LoginPage;