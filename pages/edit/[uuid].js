import React, {useEffect, useState} from 'react';
import {useRouter} from 'next/router';
import EditEvent from "@/components/ui/EditEventForm";
import {fetchEventByUuid} from "@/services/service";

const EditEventPage = () => {
    const [event, setEvent] = useState(null);
    const router = useRouter();
    const { uuid } = router.query;

    useEffect(() => {
        if (!uuid) return; // Ensure uuid is present

        (async () => {
            const eventData = await fetchEventByUuid(uuid);
            setEvent(eventData);
        })();
    }, [uuid]); // Dependency array ensures useEffect runs when uuid changes

    if (!event) {
        return <div>Loading...</div>; // Loading state
    }

    return <EditEvent eventDetails={event} />;
}

export default EditEventPage;